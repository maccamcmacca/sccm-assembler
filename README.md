# SCCM Assembler

## Instruction Set

### JMP
Unconditional jump, goes directly to the line

Usage: JMP 8(Jumps the counter to line 8)

### JRP
Relative jump, goes forward in the program by x lines

Usage: JRP 8(Jumps forward 8 lines)

### LDN
Loads the number from line x into the accumulator

Usage: LDN 8(Loads the number in line 8)

### STO
Stores the number in the accumulator to line x

Usage: STO 8(Stores the number on line 8)


### SUB
Subtracts the number at line x from the number in the accumulator and then store the result in the accumulator

Usage: SUB 8(Subtracts the number in line 8 from the accumulator and stores it in the accumulator)

### CMP
Skips the next instruction if the accumulator contains a negative value

Usage CMP 0(skips the next instruction if the accumulator holds a negative value)

### STP
Stops the program

Usage: STP 0(Stops the program)

## Install instructions

Open your terminal emulator of choice

git clone https://gitlab.com/maccamcmacca/sccm-assembler

cd sccm-assembler

make

./a.out assemblerfile

That runs the example file but you can of course run your own
