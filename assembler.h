#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <locale>
#include <cstdlib>
#include <bitset>
#include <bit>

#ifndef ASSEMBLER_H
#define ASSEMBLER_H

class assembler {
    public:
        assembler();
        ~assembler();

        bool checkDigits(std::string str);
        std::string delimiter(std::string numline, int stringNum);
        std::string parseLine(std::string line, int count);
        std::string reverseStr(std::string str);
        std::string finishString(std::string bin);
        std::string getOpcodeString(std::string opcode);
        std::string dectoBin(int dec);
        std::string endianSwap(std::string binary);
        void processFile(std::string fIlename);

        std::string getOpcode(){

                return opcode;

        }

        void setOpcode(std::string newOpcode){

                opcode = newOpcode;

        }

        std::string getOperand(){

                return operand;

        }

        void setOperand(std::string newOperand){

                operand = newOperand;

        }

    private:
        std::string opcode;
        std::string operand;

};

#endif // ASSEMBLER.H
