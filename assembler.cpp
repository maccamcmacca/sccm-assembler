#include "assembler.h"

using namespace std;

assembler::assembler(){

}

assembler::~assembler(){

}

string assembler::dectoBin(int dec)
{
    string bin = "";	//binary value
	int rem;			//remainder
	int decTemp = dec;	//temp decimal value

	int binSize; 		//size of bin value

	//make decimal positive for calculation
	if(dec < 0){
		decTemp = -decTemp;
	}

	//loop until full binary number has been created
	while(decTemp != 0)
	{
		rem = decTemp%2;
		decTemp = decTemp/2;
		if(rem == 0){
			bin = bin + '0';
		}
		else{
			bin = bin + '1';
		}
	}

	//take into account twos compliment
	if(dec < 0){
		bin = bin + '1';
	}
	else{
		bin = bin + '0';
	}
	return bin;
}

//finds out the correct opcode to assemble
string assembler::getOpcodeString(string opcode){
    int exit = 0;
    string opcodeout;
    if(opcode == "JMP"){
        opcodeout = "000";
    }else if(opcode == "JRP"){
        opcodeout = "100";
    }else if(opcode == "LDN"){
        opcodeout = "010";
    }else if(opcode == "STO"){
        opcodeout = "110";
    }else if(opcode == "SUB"){
        opcodeout = "101";
    }else if(opcode == "CMP"){
        opcodeout = "011";
    }else if(opcode == "STP"){
        opcodeout = "111";
    }else if(opcode == "NUM"){
        opcodeout = "NUM";
    }else{
        opcodeout = "ERR";
        exit = 1;
    }

   return opcodeout;
}

string assembler::finishString(string bin){

	int binSize = bin.length();

	//fill in blank operand space
	if(binSize < 33-1){
		bin.append((33-1) - (binSize),'0');
	}

    return bin;
}

bool assembler::checkDigits(string digits){

    bool digitCheck;

    for(int i = 0; i<digits.length(); i++){

        if(isdigit(digits[i])){

            digitCheck = true;
        }else if(!isdigit(digits[i])){
            digitCheck = false;
        }

    }
    return digitCheck;
}

string assembler::parseLine(string line, int count){
    //creates variables
    string opcode, operand, linenumber, concat;
    int spacePos = 0;
    //try statement for any errors
    try{
    //checks the string isn't invalid
        if(line != "" || line[0] != ' ' || line.length() != 0){
        //loops till line length is reached
        for(int i = 0; i<line.length(); i++){

            if(line[i] == ' '){

                spacePos += 1;
                if(spacePos == 2 && getOpcodeString(opcode) != "NUM"){
                    break;
                }
                if(spacePos == 3){
                    break;
                }
            }else if (spacePos == 0){

                opcode += line[i];

            }else if(spacePos == 1){
                if(getOpcodeString(opcode) == "ERR"){
                    throw 0;
                }
                operand += line[i];

            }else if(spacePos == 2 && getOpcodeString(opcode) == "NUM"){
                linenumber += line[i];
            }
        }

        opcode = getOpcodeString(opcode);

    }else{
        throw 3;
    }
    if(!checkDigits(operand)){
        throw 1;
    }

    if(getOpcodeString(opcode) == "NUM"){
        if(!checkDigits(linenumber)){
            throw 2;
        }
    }
    }catch(int e){
        if(e == 0){
            cout << "ERROR WITH OPCODE AT LINE " << count+1 <<  endl;
            exit(0);
        }else if(e == 1){
            cout << "ERROR WITH OPERAND AT LINE " << count+1 <<  endl;
            exit(0);
        }else if(e == 2){
            cout << "ERROR WITH LINE NUMBER AT LINE " << count+1 <<  endl;
            exit(0);
        }else if(e == 3){
            cout << "ERROR AT LINE " << count+1 << ", CHECK FOR SPACES" << endl;
            exit(0);
        }
}
        if(opcode == "NUM"){
            if(stoi(operand) < 0){
                concat = endianSwap(bitset<32>(stoi(operand)).to_string());
                //concat[concat.length()] = '1';
            }
            concat += ":" + linenumber + ":" + "NUM";

        }else{

            concat = dectoBin(stoi(operand));
            concat = concat.append(13 - concat.length(), '0') + opcode;
            concat = finishString(concat);
        }
        return concat;
}

//this is a very quick and hacky way of getting things done
string assembler::delimiter(string numLine, int stringNum){

    string outstring, binary, lineNumber;
    int delimiterMark = 0;

        for(int i = 0; i<numLine.length(); i++){

            if(numLine[i] == ':'){

                delimiterMark += 1;
                if(delimiterMark == 2){
                    break;
                }
            }else if (delimiterMark == 0){

                binary += numLine[i];

            }else if(delimiterMark == 1){

                lineNumber += numLine[i];

            }

        }
            if(stringNum == 0){
                outstring = binary;
            }else if(stringNum == 1){
                outstring = lineNumber;
            }
            return outstring;
}

void assembler::processFile(string filename){

    fstream newfile;
    newfile.open(filename, ios::in);
    if(!newfile){
        cout << "File provided doesn't exist" << endl;
        exit(0);
    }
    if(newfile.is_open()){
        int line;
        int count = 0;
        string mem[31];
        string fl;
        string outfile;
        try{
        while(getline(newfile, fl)){
            outfile = parseLine(fl, count);
            if(outfile.length() == 32){

            mem[count] = outfile;

            }else{

                line = stoi(delimiter(outfile, 1));
                mem[line] = delimiter(outfile, 0);
                count-=1;
            }
            count += 1;
        }
        //gets any errors and terminates the program and tells the user which line
        }catch(int e){
            if(e == 0){

                cout << "Error at line " << count+1;
                exit(0);
            }
        }
        //prints the rest of the memory
        for(int i = count; i<31; i++){
            if(mem[i] == "")
            mem[i] = "00000000000000000000000000000000";

        }
        //prints the memory to stdout
        for(int i = 0; i<31; i++){

            cout << mem[i] << endl;

        }
        newfile.close();

    }
}

string assembler::endianSwap(string binaryValue){

    string swappedEndian = binaryValue;

    for (int i=0; i < binaryValue.length(); i++){
        if (binaryValue[i] == '0'){

            swappedEndian[31 - i] = '0';

        }else{

            swappedEndian[31 - i] = '1';

        }
    }
    return swappedEndian;
}

int main(int argc, char** argv) {
    //if there isn't a command line argument print an error
    if(!argv[1]){
        cout << "You have not entered a file" << endl;
        return 0;
        //if it does exist pass it to the function
    }else if(argv[1]){
        assembler as;
        as.processFile(argv[1]);
    }
    return 0;
}
